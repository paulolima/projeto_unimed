package unimed.armazenamento;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import unimed.entidade.Filme;

public class FilmeArmazenamento {

	private static ArrayList<Filme> armazenamento;
	private static FilmeArmazenamento instance = null;

	private FilmeArmazenamento() {
		armazenamento = new ArrayList<Filme>();
		iniciarAPP();

	}

	public static ArrayList<Filme> getArmazenamento() {
		if (instance == null) {
			instance = new FilmeArmazenamento();
		}
		return armazenamento;
	}

	private static void iniciarAPP() {

		System.out.println("iniciando...");

		String arquivoCSV = "c:\\arquivo.csv";
		BufferedReader br = null;
		String linha = "";
		String csvDivisor = ",";
		int id, year;
		String title, studio1, studio2, producer1, producer2, producer3;
		boolean winner;
		try {

			br = new BufferedReader(new FileReader(arquivoCSV));
			while ((linha = br.readLine()) != null) {

				String[] dados = linha.split(csvDivisor);

				System.out.println(dados[dados.length - 9] + ", " + dados[dados.length - 8] + ", "
						+ dados[dados.length - 7] + ", " + dados[dados.length - 6] + ", " + dados[dados.length - 5]
						+ ", " + dados[dados.length - 4] + ", " + dados[dados.length - 3] + ", "
						+ dados[dados.length - 2] + ", " + dados[dados.length - 1]);

				id = Integer.parseInt(dados[dados.length - 9]);
				year = Integer.parseInt(dados[dados.length - 8]);
				title = dados[dados.length - 7];
				studio1 = dados[dados.length - 6];
				studio2 = dados[dados.length - 5];
				producer1 = dados[dados.length - 4];
				producer2 = dados[dados.length - 3];
				producer3 = dados[dados.length - 2];
				winner = Boolean.parseBoolean(dados[dados.length - 1]);

				String[] studios = new String[2];

				studios[0] = studio1;
				studios[1] = studio2;

				String[] producers = new String[3];

				producers[0] = producer1;
				producers[1] = producer2;
				producers[2] = producer3;

				Filme filme = new Filme(id, year, title, studios, producers, winner);
				armazenamento.add(filme);
			}

			System.out.println("Dados em Mem�ria");

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

}

package unimed.resources;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import unimed.armazenamento.FilmeArmazenamento;
import unimed.entidade.Filme;

@Path("/filmes")
public class FilmesResource {

	private List<Filme> filmes = new ArrayList<Filme>();

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Filme> getFilmes() {
		filmes.addAll(FilmeArmazenamento.getArmazenamento());
		return filmes;
	}

	@DELETE
	@Path("/delete/{id}")
	@Produces(MediaType.TEXT_PLAIN)
	public String remove(@PathParam("id") int id) {
		filmes.addAll(FilmeArmazenamento.getArmazenamento());

		Filme filme = null;
		String msg = "";

		for (int i = 0; i < filmes.size(); i++) {

			// verificar a existencia do filme pelo ID paramentro
			if (filmes.get(i).getId() == id) {

				// verificar se o filme nao e vencedor do oscar
				if (filmes.get(i).isWinner() == false) {

					filme = new Filme();
					filme = filmes.get(i);
					FilmeArmazenamento.getArmazenamento().remove(filme);
					msg = " O Filme = '" + filme.getTitle() + "' Foi Removido Com Sucesso!";
					
				} else {

					msg =  "O filme � vencedor, Portanto n�o pode ser Exclu�do;";
				}

			}

		} // fim do for filme

		return msg;

	}

	// -------------------------------------------------------------------------------
}

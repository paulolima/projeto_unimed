package unimed.resources;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import unimed.armazenamento.FilmeArmazenamento;
import unimed.entidade.Filme;
import unimed.entidade.Vencedor;
import unimed.entidade.Years;

@Path("/vencedores")
public class VencedorResource {

	private List<Filme> filmes = new ArrayList<Filme>();
		
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Vencedor> getVencedor() {
		
		filmes.addAll(FilmeArmazenamento.getArmazenamento());

		List<Integer> anos = new ArrayList<Integer>();
		Set<Integer> set = new HashSet<>();
		List<Vencedor> vencedores = new ArrayList<Vencedor>();
		List<Years> listaYears = new ArrayList<Years>();
		Years year = null;
		Vencedor vencedor = null;

		for (int i = 0; i < filmes.size(); i++) {

			filmes.get(i).setWinner(true);

			if (filmes.get(i).isWinner() == true) {
				int ano = filmes.get(i).getYear();
				anos.add(ano);
			}

		} // fim do for filme

		set.addAll(anos);

		for (Integer s : set) {

			int count = Collections.frequency(anos, s);

			if (count > 1) {

				// salvar os dados

				year = new Years();
				vencedor = new Vencedor();

				year.setYear(s);
				year.setWinnerCount(count);

				listaYears.add(year);

				vencedor.setYears(listaYears);

			}

		} // fim do for hashset

		vencedores.add(vencedor);

		return vencedores;
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/{year}")
	public List<Filme> selectYearQuery(@PathParam("year") int ano) {
		
		filmes.addAll(FilmeArmazenamento.getArmazenamento());
		
		List<Filme> filmesVencedores = new ArrayList<Filme>();

		for (int i = 0; i < filmes.size(); i++) {
			
			if (filmes.get(i).getYear() == ano) {

				filmesVencedores.add(filmes.get(i));
				
			}
		}

		return filmesVencedores;
	}

}

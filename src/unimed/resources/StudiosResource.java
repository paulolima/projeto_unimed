package unimed.resources;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import unimed.armazenamento.FilmeArmazenamento;
import unimed.entidade.Estudio;
import unimed.entidade.Filme;
import unimed.entidade.Studios;

@Path("/estudios")
public class StudiosResource {

	
	private List<Filme> filmes = new ArrayList<Filme>();
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public List<Estudio> getStudios() {
		
		filmes.addAll( FilmeArmazenamento.getArmazenamento());
		
		
		// criado a cole��o de nomes dos studios
		List<String> nomes = new ArrayList<String>();
		
		// criado a cole��o de studios
		List<Estudio> estudios = new ArrayList<Estudio>();
		
		List<Studios> studios = new ArrayList<Studios>();
		
		
		Set<String> set = new HashSet<>();
		
		
		// instanciado o objeto
		Estudio estudio = new Estudio();
		
		Studios studio;
	
				
		
		for(int i = 0; i < filmes.size(); i++) {
		
			String nome1 = filmes.get(i).getStudios()[0];
			nomes.add(nome1);
			String nome2 = filmes.get(i).getStudios()[1];
			nomes.add(nome2);
			
			
		}
		
		// adicionar o arraylist de nomes ao HashSet para excluir duplica��es de string
		set.addAll(nomes);
		
		//Collections.sort(nomes);   
		
		for (String s : set) {
			
			int count = Collections.frequency(nomes, s);
			
			 studio = new Studios();
			 studio.setName(s);
			 studio.setWinnerCount(count);
			 			 
			
			 studios.add(studio);
			 
			 
			 // ordena a lista de vencedores
		        Collections.sort (studios, new Comparator() {
		            public int compare(Object o1, Object o2) {
		                Studios p1 = (Studios) o1;
		                Studios p2 = (Studios) o2;
		                return p1.getWinnerCount() < p2.getWinnerCount() ? -1 : (p1.getWinnerCount() > p2.getWinnerCount() ? +1 : 0);
		            }
		        });
			 
			 
			 
			 estudio.setStudios(studios);
			 
			 
			
		}
		
		estudios.add(estudio);
		
		
		System.out.println("adfasd");
		
		//Collections.sort(estudios, new ComparadorDeVencedores()); 
		
		
		return estudios;
	}
}

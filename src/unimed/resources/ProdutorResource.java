package unimed.resources;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import unimed.armazenamento.FilmeArmazenamento;
import unimed.entidade.Filme;
import unimed.entidade.Max;
import unimed.entidade.Min;
import unimed.entidade.Produtor;
import unimed.entidade.ProdutorAno;

@Path("/produtores")
public class ProdutorResource {
	
	private List<Filme> filmes = new ArrayList<Filme>();
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Produtor getprodutores() {
		
		List<Max> listaMaxs = new ArrayList<Max>();
		List<Min> listaMins = new ArrayList<Min>();
		List<ProdutorAno> listaTodosProdutores = new ArrayList<ProdutorAno>();
		filmes.addAll( FilmeArmazenamento.getArmazenamento());
		
		ProdutorAno produtorAno = null;
		Produtor produtor = null;
		Max max = null;
		Min min = null;
		
		int maxIntervalo = 0, minIntervalo = 0;
		int maxAnterior = 0, minAnterior = 0;
		int maxPosterior = 0, minPosterior = 0;
		String maxNome = null, minNome = null;
		
		for (int i = 0; i < filmes.size(); i++) {
			
			for (int j = 0; j < filmes.get(i).getProducers().length; j++) {
				
			
				produtorAno = new ProdutorAno();
				produtorAno.setNome(filmes.get(i).getProducers()[j]);
				produtorAno.setAno(filmes.get(i).getYear());
				
				listaTodosProdutores.add(produtorAno);
			}
							
				
		}// fim do for filme
		
		

		for(int k = 0; k < listaTodosProdutores.size(); k++) {
					
			String a = listaTodosProdutores.get(k).getNome();
			
			for(int l = k+1; l < listaTodosProdutores.size(); l++) {
				
				String b = listaTodosProdutores.get(l).getNome();
			
				
				if (a.equals(b)) {
					
					
					int result = listaTodosProdutores.get(l).getAno() - listaTodosProdutores.get(k).getAno();
					
					if (maxIntervalo < result) {
						
						maxIntervalo = result;
						
						maxNome = a;
						maxAnterior = listaTodosProdutores.get(k).getAno();
						maxPosterior = listaTodosProdutores.get(l).getAno();
						
		
					}
					
					
					if (maxIntervalo > result) {
						
						
						minIntervalo = result;
						
						minNome = a;
						minAnterior = listaTodosProdutores.get(k).getAno();
						minPosterior = listaTodosProdutores.get(l).getAno();
						
					}
					
				}
				
			}
		}
		
		max = new Max();
		max.setProducer(maxNome);
		max.setInterval(maxIntervalo);
		max.setPreviousWin(maxAnterior);
		max.setFollowingWin(maxPosterior);
		
		listaMaxs.add(max);
		
		min = new Min();
		min.setProducer(minNome);
		min.setInterval(minIntervalo);
		min.setPreviousWin(minAnterior);
		min.setFollowingWin(minPosterior);
		
		listaMins.add(min);
		
		produtor = new Produtor();
		
		produtor.setMin(listaMins);
		produtor.setMax(listaMaxs);
		
		return produtor;
		
	}

}

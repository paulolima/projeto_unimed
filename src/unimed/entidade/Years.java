package unimed.entidade;

public class Years {

	private int year;
	private int winnerCount;

	public Years() {
	}

	public Years(int year, int winnerCount) {

		this.setYear(year);
		this.winnerCount = winnerCount;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getWinnerCount() {
		return winnerCount;
	}

	public void setWinnerCount(int winnerCount) {
		this.winnerCount = winnerCount;
	}
}

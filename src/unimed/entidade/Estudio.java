package unimed.entidade;

import java.util.List;

public class Estudio {
	
	private List<Studios> studios;
	
	public Estudio() {}
	
	
	public Estudio(List<Studios> studios) {
		
		this.setStudios(studios);
	}


	public List<Studios> getStudios() {
		return studios;
	}


	public void setStudios(List<Studios> studios) {
		this.studios = studios;
	}
	
	
 
}

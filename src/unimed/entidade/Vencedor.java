package unimed.entidade;

import java.util.List;

public class Vencedor {
	
	private List<Years> years;
	
	public Vencedor() {}
	
	public Vencedor(List<Years> years) {
		this.setYears(years);
		
	}

	public List<Years> getYears() {
		return years;
	}

	public void setYears(List<Years> years) {
		this.years = years;
	}

	
}

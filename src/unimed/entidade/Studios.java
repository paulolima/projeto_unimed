package unimed.entidade;

public class Studios {

	private String name;
	private int winnerCount;
	
	public Studios() {}
	
	
	public Studios(String name, int winnerCount) {
		
		this.name = name;
		this.winnerCount = winnerCount;
		
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getWinnerCount() {
		return winnerCount;
	}


	public void setWinnerCount(int winnerCount) {
		this.winnerCount = winnerCount;
	}
	
	
	
	
}

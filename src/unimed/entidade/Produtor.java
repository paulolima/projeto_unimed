package unimed.entidade;

import java.util.List;

public class Produtor {
	
	
	private List<Min>  min;
	private List<Max>  max;
	
	public Produtor() {}
	
	public Produtor(List<Min> min, List<Max> max) {
		
		this.min = min;
		this.max = max;
		
	}

	public List<Min> getMin() {
		return min;
	}

	public void setMin(List<Min> min) {
		this.min = min;
	}

	public List<Max> getMax() {
		return max;
	}

	public void setMax(List<Max> max) {
		this.max = max;
	}

}

package unimed.entidade;

public class Max {
	
	private String producer;
	private int interval;
	private int previousWin;
	private int followingWin;
	
	
	public Max() {}
	
	public Max(String producer, int interval, int previouswin, int followingWin) {
		
		this.producer = producer;
		this.interval = interval;
		this.previousWin = previouswin;
		this.followingWin = followingWin;
		
		
	}

	public String getProducer() {
		return producer;
	}

	public void setProducer(String producer) {
		this.producer = producer;
	}

	public int getInterval() {
		return interval;
	}

	public void setInterval(int interval) {
		this.interval = interval;
	}

	public int getPreviousWin() {
		return previousWin;
	}

	public void setPreviousWin(int previousWin) {
		this.previousWin = previousWin;
	}

	public int getFollowingWin() {
		return followingWin;
	}

	public void setFollowingWin(int followingWin) {
		this.followingWin = followingWin;
	}
}

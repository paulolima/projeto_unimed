Para iniciar a API e necess�rio  o jersey

o arquivo csv deve estar no disco C:\\arquivo.csv

Iniciado os testes

para melhor visualizar utilize o postman

1. Obter o(s) vencedor(es), informando um ano;
http://localhost:8080/Unimed/rest/vencedores/####
#### - informe o ano da busca

2. Obter os anos que tiveram mais de um vencedor;
http://localhost:8080/Unimed/rest/vencedores

3. Obter a lista de est�dios, ordenada pelo n�mero de premia��es;
http://localhost:8080/Unimed/rest/estudios

4. Obter o produtor com maior intervalo entre dois pr�mios, e o que obteve dois
pr�mios mais r�pido;
http://localhost:8080/Unimed/rest/produtores

5. Excluir um filme. N�o deve permitir excluir vencedores.
http://localhost:8080/Unimed/rest/filmes/delete/#
# - id do filme

Extra

para visualizar todos os filmes
http://localhost:8080/Unimed/rest/filmes